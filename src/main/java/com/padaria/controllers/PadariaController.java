package com.padaria.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.padaria.domain.Clientes;
import com.padaria.repositories.ClientesRepository;

@Controller
public class PadariaController {
	
	@Autowired
	ClientesRepository clientesRepository;
	
	
	@RequestMapping("cadastraProduto")
	public String form() {	
		Clientes cliente = new Clientes();
		this.clientesRepository.save(cliente);
		
		/* O comando Git
		 * git status
		 * git add .
		 * git commit -m "O Nome quer ser ao Commit"
		 * git push 
		 *  
		 *  
		 *  
		 *  */
		
		
		return "formEvento";
	}

}
