package com.padaria.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Clientes implements Serializable {
	private static final long serialVersionUID = 3879291890542213465L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	private String clienteNome; //Obrigatorio
	private String paiClienteNome;
	private String maeClienteNome;
	private String clienteEndereco; //Obrigatorio
	private String clienteRG;
	private String clienteCPF; //Obrigatorio 11 digitos
	private Date clienteDataNascimento; //Obrigatorio abaixo de 100 anos
 	
	public Clientes() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClienteNome() {
		return clienteNome;
	}

	public void setClienteNome(String clienteNome) {
		this.clienteNome = clienteNome;
	}

	public String getPaiClienteNome() {
		return paiClienteNome;
	}

	public void setPaiClienteNome(String paiClienteNome) {
		this.paiClienteNome = paiClienteNome;
	}

	public String getMaeClienteNome() {
		return maeClienteNome;
	}

	public void setMaeClienteNome(String maeClienteNome) {
		this.maeClienteNome = maeClienteNome;
	}

	public String getClienteEndereco() {
		return clienteEndereco;
	}

	public void setClienteEndereco(String clienteEndereco) {
		this.clienteEndereco = clienteEndereco;
	}

	public String getClienteRG() {
		return clienteRG;
	}

	public void setClienteRG(String clienteRG) {
		this.clienteRG = clienteRG;
	}

	public String getClienteCPF() {
		return clienteCPF;
	}

	public void setClienteCPF(String clienteCPF) {
		this.clienteCPF = clienteCPF;
	}

	public Date getClienteDataNascimento() {
		return clienteDataNascimento;
	}

	public void setClienteDataNascimento(Date clienteDataNascimento) {
		this.clienteDataNascimento = clienteDataNascimento;
	}
	
}
