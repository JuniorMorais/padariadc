package com.padaria.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Fornecedor implements Serializable {
	
	private static final long serialVersionUID = -2270496569196060054L;	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long forId;
	
	public String forNome;
	public String forEndereco;
	public String forCNPJ;
	
	public String getForNome() {
		return forNome;
	}
	public void setForNome(String forNome) {
		this.forNome = forNome;
	}
	public String getForEndereco() {
		return forEndereco;
	}
	public void setForEndereco(String forEndereco) {
		this.forEndereco = forEndereco;
	}
	public String getForCNPJ() {
		return forCNPJ;
	}
	public void setForCNPJ(String forCNPJ) {
		this.forCNPJ = forCNPJ;
	}
		
}
