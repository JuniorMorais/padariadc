package com.padaria.domain;

public enum TipoProduto {

	PRODUTO_LIMPESA,
	PRODUTO_ALIMENTICIO,
	PRODUTO_HIGIENE;
	
}
