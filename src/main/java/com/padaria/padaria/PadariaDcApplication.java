package com.padaria.padaria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PadariaDcApplication {

	public static void main(String[] args) {
		SpringApplication.run(PadariaDcApplication.class, args);
	}

}
