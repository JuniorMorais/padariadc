package com.padaria.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.padaria.domain.Clientes;

public interface ClientesRepository extends JpaRepository<Clientes, Long> {

	@Query(value = "select * from cliente where clienteNome = :nome", nativeQuery = true)
	Clientes pegarPeloNomedoCLiente(@Param("nome") String nomeDoCliente);
}
