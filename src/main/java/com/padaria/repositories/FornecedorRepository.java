package com.padaria.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.padaria.domain.Fornecedor;

public interface FornecedorRepository extends JpaRepository<Fornecedor, Long>{

}
