package com.padaria.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.padaria.domain.Produtos;

public interface ProdutosRepository extends JpaRepository<Produtos, Long>{	
	
}


