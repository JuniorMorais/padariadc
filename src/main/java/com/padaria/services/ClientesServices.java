package com.padaria.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.padaria.domain.Clientes;
import com.padaria.repositories.ClientesRepository;

public class ClientesServices {

	@Autowired
	ClientesRepository	clientesRepository;
	
	public Clientes criar(Clientes cliente) throws Exception {
		if (cliente.getClienteNome() == null && cliente.getClienteNome().isEmpty()) {
			throw new Exception("Nome do cliente invalido");	
		}
		return this.clientesRepository.save(cliente);
	}	
	
	@PutMapping("/{id}")
    @ResponseBody
    @Transactional
    public ResponseEntity<Clientes> updateClientes(@PathVariable(name = "id") Long id, @RequestBody Clientes clientesForm) {
        Optional<Clientes> clientesFromDatabase = clientesRepository.findById(id);

        if (clientesFromDatabase.isPresent()) {
            Clientes clientesUpdated = clientesFromDatabase.get();
            return ResponseEntity.ok(clientesUpdated);
        }
        return ResponseEntity.notFound().build();
    }
	
	@DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<Object> deleteClientes(@PathVariable Long id) {
        Optional<Clientes> clientes = clientesRepository.findById(id);
        clientes.ifPresent(value -> clientesRepository.delete(value));
        return ResponseEntity.ok().build();
    }
	
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseEntity<Clientes> getClientesById(@PathVariable Long id) {
        Optional<Clientes> clientes = clientesRepository.findById(id);
        return ResponseEntity.of(clientes);
    }	
	
    @GetMapping
    public List<Clientes> getAllClientes() {
        return clientesRepository.findAll();
    }

}
