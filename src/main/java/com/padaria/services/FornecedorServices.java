package com.padaria.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.padaria.domain.Fornecedor;
import com.padaria.repositories.FornecedorRepository;

public class FornecedorServices {
	
	@Autowired
    private FornecedorRepository fornecedorRepository;

    @GetMapping
    public List<Fornecedor> getAllAgendamentos() {
        return fornecedorRepository.findAll();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseEntity<Fornecedor> getforNomeById(@PathVariable Long id) {
        Optional<Fornecedor> forNome = fornecedorRepository.findById(id);
        return ResponseEntity.of(forNome);
    }

	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<Object> deleteFornecedor(@PathVariable Long id) {
		Optional<Fornecedor> fornecedor = fornecedorRepository.findById(id);
		fornecedor.ifPresent(value -> fornecedorRepository.delete(value));
		return ResponseEntity.ok().build();
	}

}
